﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace Calendar.DAL.UnitOfWork
{
    public static class UnitOfWorkExtensions
    {
        public static async Task ExecuteWithTransaction(
            this IUnitOfWork unitOfWork,
            Func<Task> action,
            IsolationLevel level = IsolationLevel.Serializable)
        {
            await unitOfWork.Begin(level);
            try
            {
                await action();
                await unitOfWork.Commit();
            }
            catch
            {
                await unitOfWork.Rollback();
                throw;
            }
        }

        public static async Task<TResult> ExecuteWithTransaction<TResult>(
            this IUnitOfWork unitOfWork,
            Func<Task<TResult>> func,
            IsolationLevel level = IsolationLevel.Serializable)
        {
            await unitOfWork.Begin(level);
            try
            {
                var result = await func();
                await unitOfWork.Commit();
                return result;
            }
            catch
            {
                await unitOfWork.Rollback();
                throw;
            }
        }
    }
}
