﻿using System.Data;
using System.Threading.Tasks;

namespace Calendar.DAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        Task Begin(IsolationLevel level);
        Task Commit();
        Task Rollback();
    }
}
