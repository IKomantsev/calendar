﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Calendar.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        private readonly SemaphoreSlim _semaphore;
        private int _transactionCounter;
        private bool _isRollback;
        private IDbContextTransaction _transaction;

        public UnitOfWork(DataContext context)
        {
            _context = context;

            _transactionCounter = 0;
            _isRollback = false;
            _semaphore = new SemaphoreSlim(1, 1);
        }

        public async Task Begin(IsolationLevel level)
        {
            await _semaphore.WaitAsync();
            try
            {
                _transactionCounter++;

                if (_transactionCounter == 1)
                {
                    _transaction = await _context.Database.BeginTransactionAsync(level);
                    _isRollback = false;
                }
                else if (_transactionCounter > 1 && _isRollback)
                {
                    throw new InvalidOperationException("Transaction has already been rolled back");
                }
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Commit()
        {
            await _semaphore.WaitAsync();
            try
            {
                if (_isRollback)
                {
                    throw new InvalidOperationException("Transaction has already been rolled back");
                }

                if (_transaction is null)
                {
                    throw new InvalidOperationException("There is no transaction to commit");
                }

                _transactionCounter--;

                if (_transactionCounter == 0)
                {
                    await _transaction.CommitAsync();
                }
                else if (_transactionCounter < 0)
                {
                    throw new InvalidOperationException("Nothing to commit");
                }
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Rollback()
        {
            await _semaphore.WaitAsync();
            try
            {
                _isRollback = true;

                if (_transaction is null)
                {
                    throw new InvalidOperationException("There is no transaction to commit");
                }

                _transactionCounter--;

                if (_transactionCounter == 0)
                {
                    await _transaction.RollbackAsync();
                }
                else if (_transactionCounter < 0)
                {
                    throw new InvalidOperationException("Nothing to roll back");
                }
            }
            finally
            {
                _semaphore.Release();
            }
        }
    }
}
