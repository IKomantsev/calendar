﻿using Calendar.DAL.Interfaces;
using Calendar.DAL.Mappers;
using Calendar.Domain.Calendars.Exceptions;
using Calendar.Domain.TimeSlots.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calendar.DAL.Repositories
{
    public class TimeSlotEventsRepository : ITimeSlotEventsRepository
    {
        private const string ForeignKeyErrorCode = "23503";

        private readonly DataContext _context;

        public TimeSlotEventsRepository(DataContext context)
        {
            _context = context;
        }

        public async Task Add(TimeSlotEvent timeSlotEvent)
        {
            var entity = timeSlotEvent.ToEntity();
            _context.TimeSlotEvents.Add(entity);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                var innerException = ex.InnerException as PostgresException;
                if (innerException != null)
                {
                    if (innerException.SqlState == ForeignKeyErrorCode)
                    {
                        throw new CalendarNotFoundException($"Calendar with id '{timeSlotEvent.CalendarId}' was not found");
                    }
                }
            }
        }

        public async Task DeleteByCalendarId(int calendarId)
        {
            var entities = await _context.TimeSlotEvents
                .Where(ev => ev.CalendarId == calendarId)
                .ToListAsync();

            if (entities.Count > 0)
            {
                _context.TimeSlotEvents.RemoveRange(entities);
            }

            await _context.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<TimeSlotEvent>> GetAll()
        {
            var events = await _context.TimeSlotEvents
                .AsNoTracking()
                .Select(ev => ev.ToModel())
                .ToListAsync();

            return events;
        }
    }
}
