﻿using Calendar.DAL.Interfaces;
using Calendar.DAL.Mappers;
using Calendar.Domain.Calendars.Commands;
using Calendar.Domain.Calendars.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calendar.DAL.Repositories
{
    public class CalendarsRepository : ICalendarsRepository
    {
        private readonly DataContext _context;

        public CalendarsRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<int> Create(CreateCalendarCommand command)
        {
            var entity = command.ToEntity();
            _context.Calendars.Add(entity);
            await _context.SaveChangesAsync();
            return entity.Id;
        }

        public async Task Delete(int id)
        {
            var entity = _context.Calendars.Find(id);
            if (entity == null)
            {
                throw new CalendarNotFoundException($"Calendar with id '{id}' was not found");
            }

            _context.Calendars.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<IReadOnlyCollection<int>> GetIds()
        {
            var ids = await _context.Calendars
                .Select(calendar => calendar.Id)
                .ToListAsync();

            return ids;
        }
    }
}
