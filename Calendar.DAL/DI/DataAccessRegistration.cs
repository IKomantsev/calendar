﻿using Calendar.DAL.Interfaces;
using Calendar.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Calendar.DAL.DI
{
    public static class DataAccessRegistration
    {
        public static IServiceCollection RegisterDataAccess(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContextPool<DataContext>((provider, options) =>
            {
                var connectionString = configuration.GetConnectionString(nameof(DataContext));
                options.UseNpgsql(connectionString);
            });

            services.AddScoped<ICalendarsRepository, CalendarsRepository>();
            services.AddScoped<ITimeSlotEventsRepository, TimeSlotEventsRepository>();

            services.AddScoped<UnitOfWork.IUnitOfWork, UnitOfWork.UnitOfWork>();

            return services;
        }
    }
}
