﻿using Calendar.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Calendar.DAL
{
    public class DataContext : DbContext
    {
        internal DbSet<CalendarEntity> Calendars { get; set; }
        internal DbSet<TimeSlotEventEntity> TimeSlotEvents { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
