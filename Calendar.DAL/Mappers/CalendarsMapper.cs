﻿using Calendar.DAL.Entities;
using Calendar.Domain.Calendars.Commands;

namespace Calendar.DAL.Mappers
{
    internal static class CalendarsMapper
    {
        public static CalendarEntity ToEntity(this CreateCalendarCommand command)
        {
            var entity = new CalendarEntity
            {
                Name = command.Name,
                Description = command.Description
            };

            return entity;
        }
    }
}
