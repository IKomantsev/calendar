﻿using Calendar.DAL.Entities;
using Calendar.Domain.TimeSlots.Models;
using System;

namespace Calendar.DAL.Mappers
{
    internal static class TimeSlotsMapper
    {
        public static TimeSlotEventEntity ToEntity(this TimeSlotEvent model)
        {
            var entity = new TimeSlotEventEntity
            {
                CalendarId = model.CalendarId,
                Start = model.Start,
                End = model.End,
                EventType = model.EventType,
                CreatedOn = model.CreatedOn
            };

            return entity;
        }

        public static TimeSlotEvent ToModel(this TimeSlotEventEntity entity)
        {
            var model = new TimeSlotEvent(
                CalendarId: entity.CalendarId,
                Start: entity.Start,
                End: entity.End,
                EventType: entity.EventType,
                CreatedOn: entity.CreatedOn
            );

            return model;
        }
    }
}
