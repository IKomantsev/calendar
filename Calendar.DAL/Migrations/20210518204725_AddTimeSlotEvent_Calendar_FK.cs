﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Calendar.DAL.Migrations
{
    public partial class AddTimeSlotEvent_Calendar_FK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_TimeSlotEvents_CalendarId",
                table: "TimeSlotEvents",
                column: "CalendarId");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSlotEvents_Calendars_CalendarId",
                table: "TimeSlotEvents",
                column: "CalendarId",
                principalTable: "Calendars",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TimeSlotEvents_Calendars_CalendarId",
                table: "TimeSlotEvents");

            migrationBuilder.DropIndex(
                name: "IX_TimeSlotEvents_CalendarId",
                table: "TimeSlotEvents");
        }
    }
}
