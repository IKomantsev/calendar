﻿using Calendar.Domain.Calendars.Commands;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.DAL.Interfaces
{
    public interface ICalendarsRepository
    {
        Task<int> Create(CreateCalendarCommand command);
        Task<IReadOnlyCollection<int>> GetIds();
        Task Delete(int id);
    }
}
