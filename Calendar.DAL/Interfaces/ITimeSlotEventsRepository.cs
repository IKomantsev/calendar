﻿using Calendar.Domain.TimeSlots.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.DAL.Interfaces
{
    public interface ITimeSlotEventsRepository
    {
        Task Add(TimeSlotEvent timeSlotEvent);
        Task<IReadOnlyCollection<TimeSlotEvent>> GetAll();
        Task DeleteByCalendarId(int calendarId);
    }
}
