﻿using System;

namespace Calendar.DAL.Entities
{
    internal class CalendarEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
