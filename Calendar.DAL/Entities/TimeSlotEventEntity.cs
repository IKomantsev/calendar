﻿using Calendar.Domain.TimeSlots.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Calendar.DAL.Entities
{
    internal class TimeSlotEventEntity
    {
        public int Id { get; set; }
        public int CalendarId { get; set; }
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset End { get; set; }
        public EventType EventType { get; set; }
        public DateTimeOffset CreatedOn { get; set; }

        [ForeignKey("CalendarId")]
        public CalendarEntity Calendar { get; set; }
    }
}
