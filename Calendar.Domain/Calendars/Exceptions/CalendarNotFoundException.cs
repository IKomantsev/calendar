﻿using System;

namespace Calendar.Domain.Calendars.Exceptions
{
    public class CalendarNotFoundException : Exception
    {
        public CalendarNotFoundException()
        {
        }

        public CalendarNotFoundException(string message) : base(message)
        {
        }
    }
}
