﻿namespace Calendar.Domain.Calendars.Commands
{
    public record CreateCalendarCommand(
        string Name,
        string Description);
}
