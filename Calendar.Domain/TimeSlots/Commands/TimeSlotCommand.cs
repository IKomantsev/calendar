﻿using Calendar.Domain.TimeSlots.Models;

namespace Calendar.Domain.TimeSlots.Commands
{
    public record TimeSlotCommand(int CalendarId, TimeSlot TimeSlot);
}
