﻿using System;

namespace Calendar.Domain.TimeSlots.Exceptions
{
    public class TimeSlotIntersectionException : Exception
    {
        public TimeSlotIntersectionException()
        {
        }

        public TimeSlotIntersectionException(string message) : base(message)
        {
        }
    }
}
