﻿using System;

namespace Calendar.Domain.TimeSlots.Models
{
    public record TimeSlotEvent(
        int CalendarId,
        DateTimeOffset Start,
        DateTimeOffset End,
        EventType EventType,
        DateTimeOffset CreatedOn);
}
