﻿using System;

namespace Calendar.Domain.TimeSlots.Models
{
    public record TimeSlot
    {
        public DateTimeOffset Start { get; set; }
        public DateTimeOffset End { get; set; }

        public TimeSlot(DateTimeOffset start, DateTimeOffset end)
        {
            Start = start;
            End = end;
        }
    }
}
