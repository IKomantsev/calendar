﻿namespace Calendar.Domain.TimeSlots.Models
{
    public enum EventType
    {
        Add = 0,
        Delete = 1
    }
}
