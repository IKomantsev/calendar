using Calendar.BL.Services;
using Calendar.Domain.TimeSlots.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Calendar.Tests
{
    [TestClass]
    public class TestTimeSlotsComposer
    {
        private readonly TimeSlotsComposer _composer = new();

        private static readonly TimeSlotComparer _timeSlotComparer = new();

        private static readonly DateTimeOffset H10 = new DateTimeOffset(2000, 1, 1, 10, 00, 0, TimeSpan.Zero);
        private static readonly DateTimeOffset H11 = new DateTimeOffset(2000, 1, 1, 11, 00, 0, TimeSpan.Zero);
        private static readonly DateTimeOffset H12 = new DateTimeOffset(2000, 1, 1, 12, 00, 0, TimeSpan.Zero);
        private static readonly DateTimeOffset H13 = new DateTimeOffset(2000, 1, 1, 13, 00, 0, TimeSpan.Zero);
        private static readonly DateTimeOffset H14 = new DateTimeOffset(2000, 1, 1, 14, 00, 0, TimeSpan.Zero);
        private static readonly DateTimeOffset H15 = new DateTimeOffset(2000, 1, 1, 15, 00, 0, TimeSpan.Zero);

        private static TimeSlot timeSlot_H10_H11 = new TimeSlot(H10, H11);
        private static TimeSlot timeSlot_H10_H12 = new TimeSlot(H10, H12);
        private static TimeSlot timeSlot_H10_H13 = new TimeSlot(H10, H13);
        private static TimeSlot timeSlot_H10_H14 = new TimeSlot(H10, H14);
        private static TimeSlot timeSlot_H11_H12 = new TimeSlot(H11, H12);
        private static TimeSlot timeSlot_H11_H13 = new TimeSlot(H11, H13);
        private static TimeSlot timeSlot_H11_H14 = new TimeSlot(H11, H14);
        private static TimeSlot timeSlot_H12_H13 = new TimeSlot(H12, H13);
        private static TimeSlot timeSlot_H12_H14 = new TimeSlot(H12, H14);
        private static TimeSlot timeSlot_H13_H15 = new TimeSlot(H13, H15);
        private static TimeSlot timeSlot_H14_H15 = new TimeSlot(H14, H15);

        [DynamicData(nameof(TestHasIntersectionData))]
        [DataTestMethod]
        public void TestHasIntersection(IReadOnlyCollection<TimeSlot> timeSlots, TimeSlot newTimeSlot, bool expectedResult)
        {
            var result = _composer.HasIntersection(timeSlots, newTimeSlot);
            Assert.AreEqual(expectedResult, result);
        }

        [DynamicData(nameof(TestAddTimeSlotData))]
        [DataTestMethod]
        public void TestAddTimeSlot(IReadOnlyCollection<TimeSlot> timeSlots, TimeSlot newTimeSlot, List<TimeSlot> expectedResult)
        {
            var currentTimeSlots = Copy(timeSlots);
            var timeSlot = Copy(newTimeSlot);
            _composer.AddTimeSlot(currentTimeSlots, timeSlot);
            CollectionAssert.AreEqual(expectedResult, currentTimeSlots, _timeSlotComparer);
        }

        [DynamicData(nameof(TestRemoveTimeSlotData))]
        [DataTestMethod]
        public void TestRemoveTimeSlot(IReadOnlyCollection<TimeSlot> timeSlots, TimeSlot newTimeSlot, List<TimeSlot> expectedResult)
        {
            var currentTimeSlots = Copy(timeSlots);
            var timeSlot = Copy(newTimeSlot);
            _composer.RemoveTimeSlot(currentTimeSlots, timeSlot);
            CollectionAssert.AreEqual(expectedResult, currentTimeSlots, _timeSlotComparer);
        }

        private static IEnumerable<object[]> TestHasIntersectionData
        {
            get
            {
                yield return new object[] { new[] { timeSlot_H10_H12 }, timeSlot_H11_H13, true };
                yield return new object[] { new[] { timeSlot_H10_H12 }, timeSlot_H12_H14, true };
                yield return new object[] { new[] { timeSlot_H11_H13 }, timeSlot_H10_H12, true };
                yield return new object[] { new[] { timeSlot_H12_H14 }, timeSlot_H10_H12, true };
                yield return new object[] { new[] { timeSlot_H10_H12 }, timeSlot_H13_H15, false };
                yield return new object[] { new[] { timeSlot_H13_H15 }, timeSlot_H10_H12, false };
                yield return new object[] { new[] { timeSlot_H10_H11, timeSlot_H14_H15 }, timeSlot_H12_H14, true };
                yield return new object[] { new[] { timeSlot_H10_H11, timeSlot_H14_H15 }, timeSlot_H12_H13, false };
            }
        }

        private static IEnumerable<object[]> TestAddTimeSlotData
        {
            get
            {
                yield return new object[] { new TimeSlot[0], timeSlot_H11_H13, new List<TimeSlot> { timeSlot_H11_H13 } };
                yield return new object[] { new[] { timeSlot_H10_H12 }, timeSlot_H11_H13, new List<TimeSlot> { timeSlot_H10_H13 } };
                yield return new object[] { new[] { timeSlot_H10_H12 }, timeSlot_H12_H14, new List<TimeSlot> { timeSlot_H10_H14 } };
                yield return new object[] { new[] { timeSlot_H11_H13 }, timeSlot_H10_H12, new List<TimeSlot> { timeSlot_H10_H13 } };
                yield return new object[] { new[] { timeSlot_H12_H14 }, timeSlot_H10_H12, new List<TimeSlot> { timeSlot_H10_H14 } };
                yield return new object[] { new[] { timeSlot_H10_H14 }, timeSlot_H11_H12, new List<TimeSlot> { timeSlot_H10_H14 } };
                yield return new object[] { new[] { timeSlot_H12_H13 }, timeSlot_H11_H14, new List<TimeSlot> { timeSlot_H11_H14 } };
                yield return new object[] { new[] { timeSlot_H10_H11, timeSlot_H12_H13 }, timeSlot_H11_H12, new List<TimeSlot> { timeSlot_H10_H13 } };
                yield return new object[] { new[] { timeSlot_H10_H11, timeSlot_H14_H15 }, timeSlot_H12_H13, new List<TimeSlot> { timeSlot_H10_H11, timeSlot_H14_H15, timeSlot_H12_H13 } };
            }
        }

        private static IEnumerable<object[]> TestRemoveTimeSlotData
        {
            get
            {
                yield return new object[] { new TimeSlot[0], timeSlot_H11_H13, new List<TimeSlot>() };
                yield return new object[] { new[] { timeSlot_H10_H12 }, timeSlot_H11_H13, new List<TimeSlot> { timeSlot_H10_H11 } };
                yield return new object[] { new[] { timeSlot_H10_H12 }, timeSlot_H12_H14, new List<TimeSlot> { timeSlot_H10_H12 } };
                yield return new object[] { new[] { timeSlot_H11_H13 }, timeSlot_H10_H12, new List<TimeSlot> { timeSlot_H12_H13 } };
                yield return new object[] { new[] { timeSlot_H12_H14 }, timeSlot_H10_H12, new List<TimeSlot> { timeSlot_H12_H14 } };
                yield return new object[] { new[] { timeSlot_H12_H13 }, timeSlot_H11_H14, new List<TimeSlot>() };
                yield return new object[] { new[] { timeSlot_H10_H14 }, timeSlot_H11_H12, new List<TimeSlot> { timeSlot_H10_H11, timeSlot_H12_H14 } };
                yield return new object[] { new[] { timeSlot_H10_H11, timeSlot_H12_H13 }, timeSlot_H11_H12, new List<TimeSlot> { timeSlot_H10_H11, timeSlot_H12_H13 } };
                yield return new object[] { new[] { timeSlot_H10_H11, timeSlot_H14_H15 }, timeSlot_H12_H13, new List<TimeSlot> { timeSlot_H10_H11, timeSlot_H14_H15 } };
                yield return new object[] { new[] { timeSlot_H10_H12, timeSlot_H13_H15 }, timeSlot_H11_H14, new List<TimeSlot> { timeSlot_H10_H11, timeSlot_H14_H15 } };
            }
        }

        private static List<TimeSlot> Copy(IReadOnlyCollection<TimeSlot> timeSlots)
        {
            var copy = timeSlots
                .Select(timeSlot => Copy(timeSlot))
                .ToList();

            return copy;
        }

        private static TimeSlot Copy(TimeSlot timeSlot)
        {
            var copy =  new TimeSlot(timeSlot.Start, timeSlot.End);
            return copy;
        }
    }
}
