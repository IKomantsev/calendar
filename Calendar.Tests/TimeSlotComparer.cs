﻿using Calendar.Domain.TimeSlots.Models;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Calendar.Tests
{
    internal class TimeSlotComparer : IComparer, IComparer<TimeSlot>
    {
        public int Compare(TimeSlot x, TimeSlot y)
        {
            return x.Start.CompareTo(y.Start);
        }

        public int Compare(object x, object y)
        {
            var left = x as TimeSlot;
            var right = y as TimeSlot;
            if (left == null || right == null)
            {
                throw new InvalidOperationException();
            }

            return Compare(left, right);
        }
    }
}
