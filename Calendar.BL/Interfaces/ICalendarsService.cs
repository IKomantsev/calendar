﻿using Calendar.Domain.Calendars.Commands;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.BL.Interfaces
{
    public interface ICalendarsService
    {
        Task<int> Create(CreateCalendarCommand command);
        Task<IReadOnlyCollection<int>> GetIds();
        Task Delete(int calendarId);
    }
}
