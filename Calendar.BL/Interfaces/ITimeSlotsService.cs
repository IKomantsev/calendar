﻿using Calendar.Domain.TimeSlots.Commands;
using Calendar.Domain.TimeSlots.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.BL.Interfaces
{
    public interface ITimeSlotsService
    {
        Task Create(TimeSlotCommand command);
        Task<IReadOnlyCollection<TimeSlot>> GetByCalendarId(int calendarId);
        Task Delete(TimeSlotCommand command);
        Task DeleteByCalendarId(int calendarId);
    }
}
