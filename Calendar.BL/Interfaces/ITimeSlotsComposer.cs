﻿using Calendar.Domain.TimeSlots.Models;
using System.Collections.Generic;

namespace Calendar.BL.Interfaces
{
    public interface ITimeSlotsComposer
    {
        bool HasIntersection(IReadOnlyCollection<TimeSlot> timeSlots, TimeSlot newTimeSlot);
        void AddTimeSlot(List<TimeSlot> timeSlots, TimeSlot newTimeSlot);
        void RemoveTimeSlot(List<TimeSlot> timeSlots, TimeSlot newTimeSlot);
    }
}
