﻿using Calendar.Domain.TimeSlots.Commands;
using Calendar.Domain.TimeSlots.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.BL.Interfaces
{
    public interface ITimeSlotsStorage
    {
        Task Init();
        void AddTimeSlot(TimeSlotCommand command);
        void RemoveTimeSlot(TimeSlotCommand command);
        void RemoveAllByCalendarId(int calendarId);
        IReadOnlyCollection<TimeSlot> GetAllByCalendarId(int calendarId);
    }
}
