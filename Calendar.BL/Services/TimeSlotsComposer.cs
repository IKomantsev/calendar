﻿using Calendar.BL.Interfaces;
using Calendar.Domain.TimeSlots.Models;
using System.Collections.Generic;
using System.Linq;

namespace Calendar.BL.Services
{
    public class TimeSlotsComposer : ITimeSlotsComposer
    {
        public bool HasIntersection(IReadOnlyCollection<TimeSlot> timeSlots, TimeSlot newTimeSlot)
        {
            var intersections = GetIntersections(timeSlots, newTimeSlot);
            return intersections.Count > 0;
        }

        public void AddTimeSlot(List<TimeSlot> timeSlots, TimeSlot newTimeSlot)
        {
            var intersections = GetIntersections(timeSlots, newTimeSlot);

            if (intersections.Count == 0)
            {
                timeSlots.Add(newTimeSlot);
                return;
            }

            foreach (var existingTimeSlot in intersections)
            {
                timeSlots.Remove(existingTimeSlot);

                if (existingTimeSlot.End > newTimeSlot.End)
                {
                    newTimeSlot.End = existingTimeSlot.End;
                }
                if (existingTimeSlot.Start < newTimeSlot.Start)
                {
                    newTimeSlot.Start = existingTimeSlot.Start;
                }
            }

            timeSlots.Add(newTimeSlot);
        }

        public void RemoveTimeSlot(List<TimeSlot> timeSlots, TimeSlot timeSlotToRemove)
        {
            var intersections = GetIntersections(timeSlots, timeSlotToRemove);

            if (intersections.Count == 0)
            {
                return;
            }

            foreach (var existingTimeSlot in intersections)
            {
                if (existingTimeSlot.Start < timeSlotToRemove.Start && existingTimeSlot.End > timeSlotToRemove.End)
                {
                    timeSlots.Add(new TimeSlot(existingTimeSlot.Start, timeSlotToRemove.Start));
                    timeSlots.Add(new TimeSlot(timeSlotToRemove.End, existingTimeSlot.End));
                    timeSlots.Remove(existingTimeSlot);
                }
                else if (existingTimeSlot.Start >= timeSlotToRemove.Start && existingTimeSlot.End <= timeSlotToRemove.End)
                {
                    timeSlots.Remove(existingTimeSlot);
                }
                else if (existingTimeSlot.End > timeSlotToRemove.Start && existingTimeSlot.Start < timeSlotToRemove.Start)
                {
                    existingTimeSlot.End = timeSlotToRemove.Start;
                }
                else if (existingTimeSlot.Start < timeSlotToRemove.End && existingTimeSlot.End > timeSlotToRemove.End)
                {
                    existingTimeSlot.Start = timeSlotToRemove.End;
                }
            }
        }

        private List<TimeSlot> GetIntersections(IReadOnlyCollection<TimeSlot> timeSlots, TimeSlot newTimeSlot)
        {
            var intersections = timeSlots
                .Where(timeSlot => timeSlot.Start <= newTimeSlot.End && timeSlot.End >= newTimeSlot.Start)
                .ToList();

            return intersections;
        }
    }
}
