﻿using Calendar.BL.Interfaces;
using Calendar.DAL.Interfaces;
using Calendar.Domain.TimeSlots.Commands;
using Calendar.Domain.TimeSlots.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.BL.Services
{
    public class TimeSlotsService : ITimeSlotsService
    {
        private readonly ITimeSlotsStorage _timeSlotsStorage;
        private readonly ITimeSlotEventsRepository _timeSlotEventsRepository;

        public TimeSlotsService(
            ITimeSlotsStorage timeSlotsStorage,
            ITimeSlotEventsRepository timeSlotEventsRepository)
        {
            _timeSlotsStorage = timeSlotsStorage;
            _timeSlotEventsRepository = timeSlotEventsRepository;
        }

        public async Task Create(TimeSlotCommand command)
        {
            _timeSlotsStorage.AddTimeSlot(command);
            try
            {
                await AddTimeSlotEvent(command, EventType.Add);
            }
            catch (Exception ex)
            {
                _timeSlotsStorage.RemoveTimeSlot(command);
                throw;
            }
        }

        public async Task Delete(TimeSlotCommand command)
        {
            await AddTimeSlotEvent(command, EventType.Delete);
            _timeSlotsStorage.RemoveTimeSlot(command);
        }

        public async Task DeleteByCalendarId(int calendarId)
        {
            await _timeSlotEventsRepository.DeleteByCalendarId(calendarId);
            _timeSlotsStorage.RemoveAllByCalendarId(calendarId);
        }

        public Task<IReadOnlyCollection<TimeSlot>> GetByCalendarId(int calendarId)
        {
            var timeSlots = _timeSlotsStorage.GetAllByCalendarId(calendarId);
            return Task.FromResult(timeSlots);
        }

        private Task AddTimeSlotEvent(TimeSlotCommand command, EventType eventType)
        {
            var now = DateTimeOffset.Now;

            var timeSlotEvent = new TimeSlotEvent(
                CalendarId: command.CalendarId,
                Start: command.TimeSlot.Start,
                End: command.TimeSlot.End,
                EventType: eventType,
                CreatedOn: now);

            return _timeSlotEventsRepository.Add(timeSlotEvent);
        }
    }
}
