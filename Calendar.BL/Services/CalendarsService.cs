﻿using Calendar.BL.Interfaces;
using Calendar.DAL.Interfaces;
using Calendar.DAL.UnitOfWork;
using Calendar.Domain.Calendars.Commands;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.BL.Services
{
    public class CalendarsService : ICalendarsService
    {
        private readonly ICalendarsRepository _calendarsRepository;
        private readonly ITimeSlotsService _timeSlotsService;
        private readonly IUnitOfWork _unitOfWork;

        public CalendarsService(ICalendarsRepository calendarsRepository, ITimeSlotsService timeSlotsService, IUnitOfWork unitOfWork)
        {
            _calendarsRepository = calendarsRepository;
            _timeSlotsService = timeSlotsService;
            _unitOfWork = unitOfWork;
        }

        public async Task<int> Create(CreateCalendarCommand command)
        {
            var calendarId = await _calendarsRepository.Create(command);
            return calendarId;
        }

        public async Task Delete(int calendarId)
        {
            await _unitOfWork.ExecuteWithTransaction(async () =>
            {
                await _calendarsRepository.Delete(calendarId);
                await _timeSlotsService.DeleteByCalendarId(calendarId);
            });
        }

        public async Task<IReadOnlyCollection<int>> GetIds()
        {
            var ids = await _calendarsRepository.GetIds();
            return ids;
        }
    }
}
