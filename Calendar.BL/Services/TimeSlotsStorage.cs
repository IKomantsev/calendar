﻿using Calendar.BL.Interfaces;
using Calendar.DAL.Interfaces;
using Calendar.Domain.TimeSlots.Commands;
using Calendar.Domain.TimeSlots.Exceptions;
using Calendar.Domain.TimeSlots.Models;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calendar.BL.Services
{
    public class TimeSlotsStorage : ITimeSlotsStorage
    {
        private readonly Dictionary<int, List<TimeSlot>> _timeSlots = new();

        private readonly ITimeSlotsComposer _timeSlotsComposer;
        private readonly IServiceScopeFactory _scopeFactory;
        //private readonly ITimeSlotEventsRepository _timeSlotEventsRepository;

        public TimeSlotsStorage(ITimeSlotsComposer timeSlotsComposer, IServiceScopeFactory scopeFactory)
        {
            _timeSlotsComposer = timeSlotsComposer;
            _scopeFactory = scopeFactory;
        }

        public async Task Init()
        {
            using var scope = _scopeFactory.CreateScope();
            var timeSlotEventsRepository = scope.ServiceProvider.GetRequiredService<ITimeSlotEventsRepository>();

            var allEvents = await timeSlotEventsRepository.GetAll();

            var eventsByCalendar = allEvents
                .GroupBy(ev => ev.CalendarId)
                .ToDictionary(gr => gr.Key, gr => gr.OrderBy(ev => ev.CreatedOn).ToList());

            foreach (var calendarEvents in eventsByCalendar)
            {
                var calendarId = calendarEvents.Key;
                var events = calendarEvents.Value;
                var timeSlots = new List<TimeSlot>();

                foreach (var ev in events)
                {
                    var timeSlot = new TimeSlot(ev.Start, ev.End);

                    if (ev.EventType == EventType.Add)
                    {
                        _timeSlotsComposer.AddTimeSlot(timeSlots, timeSlot);
                    }
                    if (ev.EventType == EventType.Delete)
                    {
                        _timeSlotsComposer.RemoveTimeSlot(timeSlots, timeSlot);
                    }
                }

                _timeSlots.Add(calendarId, timeSlots);
            }
        }

        public void AddTimeSlot(TimeSlotCommand command)
        {
            var timeSlots = GetTimeSlotsByCalendarId(command.CalendarId);

            if (_timeSlotsComposer.HasIntersection(timeSlots, command.TimeSlot))
            {
                throw new TimeSlotIntersectionException();
            }

            _timeSlotsComposer.AddTimeSlot(timeSlots, command.TimeSlot);
        }

        public void RemoveTimeSlot(TimeSlotCommand command)
        {
            var timeSlots = GetTimeSlotsByCalendarId(command.CalendarId);
            _timeSlotsComposer.RemoveTimeSlot(timeSlots, command.TimeSlot);
        }

        public void RemoveAllByCalendarId(int calendarId)
        {
            _timeSlots.Remove(calendarId);
        }

        public IReadOnlyCollection<TimeSlot> GetAllByCalendarId(int calendarId)
        {
            if (!_timeSlots.TryGetValue(calendarId, out var timeSlots))
            {
                timeSlots = new List<TimeSlot>();
            }

            return timeSlots.OrderBy(ts => ts.Start).ToList();
        }

        private List<TimeSlot> GetTimeSlotsByCalendarId(int calendarId)
        {
            if (!_timeSlots.TryGetValue(calendarId, out var timeSlots))
            {
                timeSlots = new List<TimeSlot>();
                _timeSlots.Add(calendarId, timeSlots);
            }

            return timeSlots;
        }
    }
}
