﻿using Calendar.BL.Interfaces;
using Calendar.BL.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Calendar.BL.DI
{
    public static class BusinessLogicRegistration
    {
        public static IServiceCollection RegisterBusinessLogic(this IServiceCollection services)
        {
            services.AddScoped<ICalendarsService, CalendarsService>();
            services.AddScoped<ITimeSlotsService, TimeSlotsService>();
            services.AddSingleton<ITimeSlotsStorage, TimeSlotsStorage>();
            services.AddSingleton<ITimeSlotsComposer, TimeSlotsComposer>();

            return services;
        }
    }
}
