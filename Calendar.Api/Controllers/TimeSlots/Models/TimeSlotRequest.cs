﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Calendar.Api.Controllers.TimeSlots.Models
{
    public record TimeSlotRequest
    {
        [Required]
        public int? CalendarId { get; init; }
        [Required]
        public DateTimeOffset? Start { get; init; }
        [Required]
        public DateTimeOffset? End { get; init; }
    }
}
