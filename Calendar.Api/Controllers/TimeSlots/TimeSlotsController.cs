﻿using Calendar.Api.Controllers.TimeSlots.Exceptions;
using Calendar.Api.Controllers.TimeSlots.Models;
using Calendar.BL.Interfaces;
using Calendar.Domain.Calendars.Exceptions;
using Calendar.Domain.TimeSlots.Exceptions;
using Calendar.Domain.TimeSlots.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.Api.Controllers.TimeSlots
{
    public class TimeSlotsController : Controller
    {
        private readonly ITimeSlotsService _timeSlotsService;

        public TimeSlotsController(ITimeSlotsService timeSlotsService)
        {
            _timeSlotsService = timeSlotsService;
        }

        [HttpGet("api/v1/timeslots")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IReadOnlyCollection<TimeSlot>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetByCalendarId([FromQuery] int calendarId)
        {
            try
            {
                var timeSlots = await _timeSlotsService.GetByCalendarId(calendarId);
                return Ok(timeSlots);
            }
            catch (CalendarNotFoundException ex)
            {
                return NotFound();
            }
        }

        [HttpPost("api/v1/timeslots")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Create([FromBody] TimeSlotRequest request)
        {
            try
            {
                TimeSlotsValidator.Validate(request);

                var command = request.ToCommand();
                await _timeSlotsService.Create(command);
                return Ok();
            }
            catch (TimeSlotsValidationException ex)
            {
                return BadRequest();
            }
            catch (CalendarNotFoundException ex)
            {
                return NotFound();
            }
            catch (TimeSlotIntersectionException ex)
            {
                return Conflict();
            }
        }

        [HttpDelete("api/v1/timeslots")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete([FromBody] TimeSlotRequest request)
        {
            try
            {
                TimeSlotsValidator.Validate(request);

                var command = request.ToCommand();
                await _timeSlotsService.Delete(command);
                return Ok();
            }
            catch (TimeSlotsValidationException ex)
            {
                return BadRequest();
            }
            catch (CalendarNotFoundException ex)
            {
                return NotFound();
            }
            catch (TimeSlotIntersectionException ex)
            {
                return Conflict();
            }
        }
    }
}
