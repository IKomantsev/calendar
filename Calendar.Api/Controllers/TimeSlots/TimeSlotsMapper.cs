﻿using Calendar.Api.Controllers.TimeSlots.Models;
using Calendar.Domain.TimeSlots.Commands;
using Calendar.Domain.TimeSlots.Models;

namespace Calendar.Api.Controllers.TimeSlots
{
    internal static class TimeSlotsMapper
    {
        public static TimeSlotCommand ToCommand(this TimeSlotRequest request)
        {
            var timeSlot = new TimeSlot(request.Start.Value, request.End.Value);

            var command = new TimeSlotCommand(
                CalendarId: request.CalendarId.Value,
                TimeSlot: timeSlot);

            return command;
        }
    }
}
