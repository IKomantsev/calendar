﻿using Calendar.Api.Controllers.TimeSlots.Exceptions;
using Calendar.Api.Controllers.TimeSlots.Models;

namespace Calendar.Api.Controllers.TimeSlots
{
    internal class TimeSlotsValidator
    {
        public static void Validate(TimeSlotRequest request)
        {
            if (request.Start >= request.End)
            {
                throw new TimeSlotsValidationException($"{nameof(request.Start)} should be less than {nameof(request.End)}");
            }
        }
    }
}
