﻿using System;

namespace Calendar.Api.Controllers.TimeSlots.Exceptions
{
    internal class TimeSlotsValidationException : Exception
    {
        public TimeSlotsValidationException()
        {
        }

        public TimeSlotsValidationException(string message) : base(message)
        {
        }
    }
}
