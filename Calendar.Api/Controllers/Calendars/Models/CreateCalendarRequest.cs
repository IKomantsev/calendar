﻿using System.ComponentModel.DataAnnotations;

namespace Calendar.Api.Controllers.Calendars.Models
{
    public record CreateCalendarRequest
    {
        [Required]
        public string Name { get; init; }
        public string Description { get; init; }
    }
}
