﻿using Calendar.Api.Controllers.Calendars.Models;
using Calendar.Domain.Calendars.Commands;

namespace Calendar.Api.Controllers.Calendars
{
    internal static class CalendarsMapper
    {
        public static CreateCalendarCommand ToCommand(this CreateCalendarRequest request)
        {
            var command = new CreateCalendarCommand(
                Name: request.Name,
                Description: request.Description);

            return command;
        }
    }
}
