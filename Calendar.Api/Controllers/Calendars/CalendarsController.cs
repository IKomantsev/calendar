﻿using Calendar.Api.Controllers.Calendars.Models;
using Calendar.BL.Interfaces;
using Calendar.Domain.Calendars.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendar.Api.Controllers.Calendars
{
    public class CalendarsController : Controller
    {
        private readonly ICalendarsService _calendarsService;

        public CalendarsController(ICalendarsService calendarsService)
        {
            _calendarsService = calendarsService;
        }

        [HttpGet("api/v1/calendars/ids")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IReadOnlyCollection<int>))]
        public async Task<IActionResult> GetIds()
        {
            var calendarIds = await _calendarsService.GetIds();
            return Ok(calendarIds);
        }

        [HttpPost("api/v1/calendars")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(int))]
        public async Task<IActionResult> Create([FromBody] CreateCalendarRequest request)
        {
            var command = request.ToCommand();
            var id = await _calendarsService.Create(command);
            return Ok(id);
        }

        [HttpDelete("api/v1/calendars/{calendarId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete([FromRoute] int calendarId)
        {
            try
            {
                await _calendarsService.Delete(calendarId);
                return Ok();
            }
            catch (CalendarNotFoundException ex)
            {
                return NotFound();
            }
        }
    }
}
